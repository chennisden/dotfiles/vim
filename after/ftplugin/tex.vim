let g:tex_flavor = "latex"
:command W silent !dlatexmk %
:map <F4> :%s/\.\$/$./ge\|:%s/\,\$/$,/ge<CR>
